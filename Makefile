.PHONY: clean all

all:	m3.pdf

clean:	
	rm -f m3.pdf
	rm -f m3.log
	rm -f m3.aux
	rm -f m3.toc
	rm -f m3.out

m3.pdf: m3.tex
	pdflatex m3.tex
